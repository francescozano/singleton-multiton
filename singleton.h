#ifndef SINGLETON_H
#define SINGLETON_H

#include <list>

using namespace std;

//Generics Singleton
template <typename T> class Singleton{

    public:
        list<T> getData(){
            return data;
        }
        static Singleton& LazyInstance(){
            static Singleton<T> sing;
            return sing;
        }
        void Write(T toAdd){
            data.push_back(toAdd);
        }
    protected:
        ~Singleton() {}
        Singleton(){}
        static list<T> data;
};

template <typename T>
list<T> Singleton<T>::data={};

#endif // SINGLETON_H
