#ifndef LOG_H
#define LOG_H
#include <list>
#include <string>
#include <iostream>
#include <singleton.h>

using namespace std;

class Log: public Singleton<string>{
    private:
        //constructor hidden fro the user cause nobody
        //can create this class except this class
        Log();
        static Log* inst;
    public:
        //To get the instance
        static Log* Instance(){
            //if it existe i return it, if not i create it
            if(!inst)
                inst=new Log;
            return inst;
        }

        static Log& LazyInstance(){
            return (Log&)Singleton::LazyInstance();
        }
        //to show the list
        void Show(){
            cout<<endl;
            for (string st: getData())
                cout<<(st)<<endl;
        }
};

#endif // LOG_H
