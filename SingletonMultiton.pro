QT += core
QT -= gui

CONFIG += c++11

TARGET = SingletonMultiton
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    log.cpp \
    singleton.cpp \
    multiton.cpp

HEADERS += \
    log.h \
    singleton.h \
    multiton.h
