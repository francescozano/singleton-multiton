#include <QCoreApplication>
#include "log.h"
#include "multiton.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Log* log;
    log=Log::Instance();
    char c=' ';
    log->Write("Log Created");
    log->Write("What's next?");
    log->Show();
    cin>>c;
    log->Write("deleting variable");
    log->Show();
    delete log;
    cin>>c;
    log=Log::Instance();
    log->Write("Back again");
    log->Show();
    cin>>c;

    //the problem with this pattern is that the variable instance in tha class is never destructed
    //so we can use the Lazy instance to destroy the refence every time

    Log log2=Log::LazyInstance();
    log2.Write("Created Lazy Log");
    log2.Write("Adding something");
    Log::LazyInstance().Show();
    cin>>c;

    Multiton<int,string>* mul1=Multiton<int,string>::getInstance(1);
    Multiton<int,string>* mul2=Multiton<int,string>::getInstance(2);
    mul1->Write("Works");
    mul2->Write("Fine");

    mul1->Show();
    mul2->Show();

    return a.exec();
}
