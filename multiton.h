#ifndef MULTITON_H
#define MULTITON_H

#include <map>
#include <list>
#include <iostream>

using namespace std;

template <typename Key,typename T> class Multiton{
    public:
        ~Multiton() {}
        // function to destroy all the obejcts.
        // It is like the destructor of the class.
         static void destroy(){
            for (typename map<Key, Multiton<Key,T>*>::const_iterator it = inst.begin(); it != inst.end(); ++it)
                delete (*it).second;
            inst.clear();
        }

        static Multiton<Key,T>* getInstance(const Key& key){
            typename map<Key, Multiton<Key,T>*>::const_iterator it = inst.find(key);

            // return the object if it exist for the given key
            if (it != inst.end()) {
                return (Multiton<Key,T>*)(it->second);
            }

            // Else create a new object and return its instance.
            inst[key] = new Multiton<Key,T>;
            return inst[key];
        }


        void Write(T toAdd){
            data.push_back(toAdd);
        }

        void Show(){
            cout<<endl;
            for (T obj : data)
                cout<<obj<<endl;
        }
    protected:
        Multiton(){data={};}
        list<T> data;
        static map<Key, Multiton<Key,T>*> inst;

};

template <typename Key,typename T>
std::map<Key,Multiton<Key,T>*> Multiton<Key,T>::inst;


#endif // MULTITON_H
